%% Praktikumsversuch inverses Pendel
% Modellparameter entsprechend Anleitung:
mp=0.325;           % Punktmasse des Pendels in kg
mw=3.662;           % Punktmasse des Wagens in kg
lsp=.451;           % Länge des Pendelstabes in m
ur=5.198;           %1.485;      % Faktor v-abh. Reibung
urh=13.57           %3.878;   % Trockenreibung
load("lookuptablecoloumb.mat")
%urh=0;             % ortsabhängige Reibung = 0.
cr=.0011            %;.00145; % Faktor Gleitreibung Pendel
cr=0.0055
g=9.81;             % Erdbeschleunigung in m/s²
%JA=0.08433;         % Massenträgheitsmoment Pendel
JA=0.6518
%unsere Modifikationen
max_weg=0.3;        %Maximale Wegstrecke

g=9.81;

FMax = (11*3.2)%-urh);  % Maximale Stellkraft in N
virt_Feder=11*3.2;    %Virtuelle Federkraft zur Wegbegrenzung
FMaxcontroller = (20*3.5)%-urh)
%% Zustandsraummodell
% (linearisierte) Massenmatrix:
MassMatrix = [mp+mw, mp*lsp;mp*lsp, JA];

% Zustandsvektor in vorläufiger Sortierung: x = [x, phi, xdot, phidot]'
% Bewgungsgleichung M*[xddot,phiddot]' = Cmech*[x,phi]'
Cmech = [-urh, 0, -ur, 0; 0,mp*lsp*g,0, -cr];
A_IP = diag(ones(2,1),2); % Zusammenhang Position-Geschwindigkeit
A_IP(3:4,:) = MassMatrix\Cmech; % Bewegungsgleichung auflösen nach Beschleunigung
% Umsortieren des Zustandsvektors: [x,xdot, phi,phidot]:
A = A_IP([1,3,2,4],[1,3,2,4]);
% Eingangsmatrix:
B_IP = [[0,0]';inv(MassMatrix) * [1,0]']; 
B = B_IP([1,3,2,4]);
% LTI-Zustandsraummodell:
sysPendel = ss(A,B,eye(4),[]);
C=eye(4);
clear A_IP B_IP

%% Diskretisierung
% sample time:
Ts = 0.015;
sysPendelDiscr = c2d(sysPendel,Ts,'zoh');

%% Zustandsrückführung mit Polvorgabe
% dominierendes Polpaar mit Realteil -2 --> Ausregelzeit etwa 2 s
Poles = [-3.5+2.6i, -2.5-2.6i, -4, -10]; 
%Poles=[-4+2i, -4-2i, -5, -5];
PolesDiscrete = exp(Poles*Ts); % pole matching z=exp(s*Ts)
% Vorschlag aus Praktikumsanleitung für diskrete Pollage: [0.9 0.85 0.9 0.85];  

Kdiscr1 = acker(sysPendelDiscr.A, sysPendelDiscr.B,PolesDiscrete)
V=-1/(C*(A-B*Kdiscr1)^-1*B);
V2=V(1,1)



%%
% Gewichtung LQR
Q = [10,0,0,0; % Zustandsgewichtung
    0,0,0,0;
    0,0,1,0;
    0,0,0,0];
R = 1
[K S P]=lqr(A,B,Q,R)
Kdiscr =  lqr(A,B,Q,R);
V=-1/(C*(A-B*Kdiscr)^-1*B);
V2=V(1,1)


%% Plots



t = out.scopeout.time
F = out.scopeout.signals(1).values
x = out.scopeout.signals(2).values;
xdot = out.scopeout.signals(3).values;
phi = out.scopeout.signals(4).values(:)
phidot = out.scopeout.signals(5).values;
t2 = out.scopeout_2.time
E = out.scopeout_2.signals(1).values;
f1=figure(1)
f1.Position = [0 0 800 500]

clf(f1)
subplot(5,1,1)
plot(t,F,"linewidth",1.5)
grid on
ylim([-80 80])
ylabel("F [N]")

subplot(5,1,2)
plot(t,x,"linewidth",1.5)
grid on
hold on
yline(0.5,'-r')
yline(-0.5,'-r')
ylabel("x [m]")
ylim([-0.65 0.65])

subplot(5,1,3)
plot(t,xdot,"linewidth",1.5)
grid on
ylim([-2.5 2.5])
ylabel('xw [m/s]')

subplot(5,1,4)
plot(t,phi,"linewidth",1.5)
grid on
ylabel("phi [°]")

subplot(5,1,5)
plot(t,phidot,"linewidth",1.5)
grid on
ylabel("phidot [rad/s]")
xlabel("t [s]")
grid on
% subplot(6,1,6) % habe Energie mal rausgenommen
% plot(t2,E,"linewidth",1.5)
% grid on
% ylabel("E [J]")
% xlabel("t [s]")
% grid on
Folder = cd;
Folder = fullfile(Folder, '..');
Folder = fullfile(Folder, 'Bilder\Matlabplots\Benny');
filename = 'Methode'+string(out.select);
exportgraphics(f1,fullfile(Folder, filename+'_Simulation.png'),'BackgroundColor','none')
savefig(f1,fullfile(Folder, filename + '_Simulation.fig'))




