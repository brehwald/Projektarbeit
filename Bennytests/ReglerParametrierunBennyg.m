%% Praktikumsversuch inverses Pendel
% Modellparameter entsprechend Anleitung:
mp=0.325;           % Punktmasse des Pendels in kg
mw=3.662;           % Punktmasse des Wagens in kg
lsp=.451;           % Länge des Pendelstabes in m
ur=5.198;           %1.485;      % Faktor v-abh. Reibung
urh=13.57           %3.878;   % Trockenreibung
%urh=0;             % ortsabhängige Reibung = 0.
cr=.0011;           %;.00145; % Faktor Gleitreibung Pendel
g=9.81;             % Erdbeschleunigung in m/s²
JA=0.08433;         % Massenträgheitsmoment Pendel
%JA=0.06
%unsere Modifikationen
max_weg=0.4;        %Maximale Wegstrecke

g=9.81;

load("lookuptablecoloumb.mat")

%% Zustandsraummodell
% (linearisierte) Massenmatrix:
MassMatrix = [mp+mw, mp*lsp;mp*lsp, JA];

% Zustandsvektor in vorläufiger Sortierung: x = [x, phi, xdot, phidot]'
% Bewgungsgleichung M*[xddot,phiddot]' = Cmech*[x,phi]'
Cmech = [-urh, 0, -ur, 0; 0,mp*lsp*g,0, -cr];
A_IP = diag(ones(2,1),2); % Zusammenhang Position-Geschwindigkeit
A_IP(3:4,:) = MassMatrix\Cmech; % Bewegungsgleichung auflösen nach Beschleunigung
% Umsortieren des Zustandsvektors: [x,xdot, phi,phidot]:
A = A_IP([1,3,2,4],[1,3,2,4]);
% Eingangsmatrix:
B_IP = [[0,0]';inv(MassMatrix) * [1,0]']; 
B = B_IP([1,3,2,4]);
% LTI-Zustandsraummodell:
sysPendel = ss(A,B,eye(4),[]);
C=eye(4);
clear A_IP B_IP

%% Diskretisierung
% sample time:
Ts = 0.015;
sysPendelDiscr = c2d(sysPendel,Ts,'zoh');


%% Gewichtung LQR
Q = [0.0000001,0,0,0; % Zustandsgewichtung
    0,0,0,0;
    0,0,0.0000000000000001,0;
    0,0,0,0];
R = 1;
[K S P]=lqr(A,B,Q,R);
%[K S P]=lqr(sysPendelDiscr.A,sysPendelDiscr.B,Q,R);
Kdiscr =  lqr(A,B,Q,R)
V=-1/(C*(sysPendelDiscr.A-sysPendelDiscr.B*Kdiscr)^-1*sysPendelDiscr.B);
V2=V(1,4)


p=(C*(eye(4)-sysPendelDiscr.A+sysPendelDiscr.B*Kdiscr)^-1*sysPendelDiscr.B).^-1;


%% In Realität 
K_lqr=[-110 -75 -281 -60];
V_lqr=-110;

%% Plots


;
t = out.scopeout.time;;
step = out.scopeout.signals(1).values;;
x = out.scopeout.signals(2).values;;
xdot = out.scopeout.signals(3).values;;
phi = out.scopeout.signals(4).values(:);
phidot = out.scopeout.signals(5).values;;
;
f1=figure(1);;
f1.Position = [0 0 800 500];;
clf(f1);;
subplot(5,1,1);
plot(t,step,"linewidth",1.5);
ylim([0 0.3]);
grid on;
ylabel("x_{soll} [m]");
subplot(5,1,2);
plot(t,x,"linewidth",1.5);
grid on;
ylabel("x [m]");
subplot(5,1,3);
plot(t,xdot,"linewidth",1.5);
grid on;
ylabel("xw [m/s]");
subplot(5,1,4);
plot(t,phi,"linewidth",1.5);
grid on;
ylabel("phi [°]");
subplot(5,1,5);
plot(t,phidot,"linewidth",1.5);
grid on;
ylabel("phidot [rad/s]");
xlabel("t [s]");
;
grid on;
Folder = cd;;
Folder = fullfile(Folder, '..');;
Folder = fullfile(Folder, 'Bilder\Matlabplots');;
exportgraphics(f1,fullfile(Folder, 'Reglersimantwort.png'),'BackgroundColor','none');